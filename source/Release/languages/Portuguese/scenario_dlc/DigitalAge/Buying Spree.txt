[HEADER]
Title=On a Buying Spree
Description=Your mission is to build a digital powerhouse by strategically acquiring eight high-potential tech companies.^^Each acquisition unlocks unique technologies and top-tier talent, offering the opportunity to create transformative synergies and achieve economies of scale.^^The clock is ticking—startup valuations are skyrocketing, and the window to secure these assets is rapidly narrowing. Can you act swiftly and execute your consolidation strategy before these rising stars slip beyond your financial grasp?

DLC=DigitalAge

Difficulty Rating=110

[ENVIRONMENT]
Number of Cities=5
Your Start-up Capital=Very High
Random Events=Occasional 
Game Starting Year=1990
Retail Store Type=Many
Technology Disruption=On
Stock Market=Yes
Alternative Stock Sim=Yes
Boom-Bust Cycle Volatility=Moderate
Macro Economy Realism=High
Inflation=Inverse
Inflation Strength=Normal

[CITIES]
City=San Francisco 
City=Toronto 
City=Tel Aviv 
City=Sydney
City=Seattle 

[COMPETITORS]
Number of Competitors=35
Competitor Start-up Capital=Moderate  
AI Expansion Aggressiveness=Moderate
AI Pricing Aggressiveness=Moderate 
AI Expertise Level=Moderate
AI Tech Head Start=None
Show Competitor Trade Secrets=Yes
AI Friendly Merger=No
Competence of Local Competitors=Moderate 

[IMPORTS]
Consumer Goods Seaports=2
Industrial Goods Seaports=2
Constant Import Supply=No
Import Quality=Moderate 

[DIGITAL AGE DLC]
AI Acquires Private Companies=Never
AI Tech Companies Go IPO=No
Max AI software and Internet companies=High
Software Revenue Index=100
Internet Revenue Index=100
E-Commerce=Yes
Talent System = Full
More Talents = 0
Disruption to Traditional Media=None
Telecom Customer Stickiness = 100

//-------------------------------------------------------------------//

[MAIN GOAL]
Goal Title=On a Buying Spree
Goal Description=The goal of this scenario is to acquire a total of eight private tech companies and consolidate them to reap the benefits of synergy and economies of scale. 

Number of Game Years=70

[MAIN GOAL VALUES]
Private Acquisitions=8

[MAIN GOAL REWARDS] 
Score change=350

//-------------------------------------------------------------------//

[CHALLENGE GAME]
Number of Game Years=30
Ranking Method=Score

//-------------------------------------------------------------------//

[CHALLENGE GAME]
Number of Game Years=30
Ranking Method=Goal+Score

